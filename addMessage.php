<?php

require "app/Autoloader/Autoloader.php";

//data from form
$errorMessage = null;
$name         = htmlspecialchars($_POST['name'], ENT_QUOTES);
$email        = htmlspecialchars($_POST['email'], ENT_QUOTES);
$message      = htmlspecialchars($_POST['message'], ENT_QUOTES);
$time         = date("Y-m-d H:i:s");

//Validation
if (empty($name) || strlen($name) > 50) {
    $errorMessage = "Enter Your Name.
                    Name should contain at least one character.
                    Name can't contain more than 50 characters";
}

elseif (empty($message)) {
    $errorMessage = "There is no need to send empty comment";
}


if ($errorMessage != null) {
    echo json_encode(['errorMessage' => $errorMessage]);
} else {

//PDO:
    $db = new App\Database\Database();

    $lastId = $db->addComment($name, $message, $email);

    echo json_encode(['errorMessage' => $errorMessage,
        'name'    => $name,
        'date'    => $time,
        'message' => $message,
        'id'      => $lastId]);
}


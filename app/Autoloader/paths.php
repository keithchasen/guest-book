<?php

define("PATH", $_SERVER['DOCUMENT_ROOT']);

return array(
    'App\Database' => PATH . "/app/Database/",
    'App\Models'   => PATH . "/app/models/",
    'App'          => PATH . "/app/"
);
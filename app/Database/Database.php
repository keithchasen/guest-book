<?php
namespace App\Database;

use PDO;
use PDOException;

require "database.php";

class Database
{

    private $db;
    private $error;

    public function __construct()
    {
        try {
            $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
        } catch(PDOException $e){
            $this->error = $e->getMessage();
        }
    }


    /**
     * function
     *getComments()
     *
     * getting all comments from DB and return it as ASSOC ARRAY
     */
    public function getComments()
    {
        $sth = $this->db->prepare("SELECT c.id, u.name, c.comment, c.date
                                 FROM user u, comment c
                                 WHERE c.user_id = u.id");
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();
        $data = $sth->fetchAll();
        return $data;
    }

    /**
     * function
     * addComment()
     *
     * adding comment
     *
     * Returns: id of comment
    */

    public function addComment($name, $message, $email)
    {
        $name = htmlspecialchars($name, ENT_QUOTES);
        $message = htmlspecialchars($message, ENT_QUOTES);
        $email = htmlspecialchars($email, ENT_QUOTES);

        //Checking if there a record with person commented

        $userId = $this->db->prepare("SELECT id
                        FROM user
                        WHERE
                        name ='$name'
                        AND email = '$email'");
        $userId->setFetchMode(PDO::FETCH_NUM);
        $userId->execute();
        $userId = $userId->fetchColumn();

//Adding message to DB

        $insertMessage = $this->db->prepare("INSERT INTO
                     comment (user_id, comment) VALUES ('$userId', '$message')");
        $insertMessage->execute();
        $lastId = $this->db->lastInsertId();
        return $lastId;
    }

    /**
     * function
     * deleteComment()
     *
     * deleting coment
     *
     * gets deleteIteId
    */

    public function deleteComment($deleteItemId)
    {
        $deleteSQL = $this->db->prepare("DELETE FROM comment WHERE id = '$deleteItemId'");
        $deleteSQL->execute();
    }

    /**
     * function
     * signIn()
     *
     * creates a new user
     *
     */

    public function signIn($name, $email, $pass)
    {
        //checking if there a user with such login or email:
        $checkLoginEmail = $this->db->prepare("SELECT COUNT(*)
                                                            FROM user
                                                            WHERE
                                                            name = '$name'
                                                            OR email = '$email'");

        $checkLoginEmail->setFetchMode(PDO::FETCH_NUM);
        $checkLoginEmail->execute();
        $isThereUser = $checkLoginEmail->fetchColumn();

        //If there is such user returning error
        if ((int)$isThereUser > 0) {
            //if there was error in input returning error message
            $errorMessage = "User with such login or email already exists.";
        } else {
            //else -> Adding user
            //encrypt password:
            $pass = md5($pass);

            $sth = $this->db->prepare("INSERT INTO user
                    (name, password, email) VALUES ('$name', '$pass', '$email')");
            $sth->execute();
        }

        return $errorMessage;
    }

    /**
     * function
     * logIn()
     *
     * checks if there such user
     * if yes creates a session for user
     */
    public function logIn($login, $pass)
    {
        $error = null;
        $pass = md5($pass);

        try {
            $sth = $this->db->prepare("SELECT * FROM user WHERE
                        name = '$login' AND password = '$pass';");

            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $sth->execute();
            $count  = $sth->fetch();
        } catch(PDOException $e) {
        $error = $e->getMessage();
            }

        if ((int)$count > 0) {
            $_SESSION['loggedIn'] = true;
            $_SESSION["name"]     = $count['name'];
            $_SESSION['email']    = $count['email'];
        } else {
            $error = "Cannot login. Please check you login and password";
        }

        return $error;
    }
}
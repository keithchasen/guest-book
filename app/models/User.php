<?php
namespace App\Models;

class User extends \App\Model
{
      public function signIn($name, $email, $pass)
    {
        $error = $this->db->signIn($name, $email, $pass);
        return $error;
    }

    public function logIn($name, $pass)
    {
        $error = $this->db->logIn($name, $pass);
        return $error;
    }
    
}
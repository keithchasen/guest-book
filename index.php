<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php
session_start();
if (isset($_SESSION['loggedIn'])): ?>

    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h1>Guest Book</h1>
            </div>
        </div>
    </div>

    <div class="container">

    <div class="form well">

        <!--Form-->
        <form role="form" name="form" id="form">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-2">
                        <label for="">User:
                            <div id="userName">
                                <?php echo $_SESSION['name']; ?>
                                </div>
                        </label>

                        <label for="">Email:
                            <div id="userEmail">
                                <?php echo $_SESSION['email']; ?>
                            </div>
                        </label>
                    </div>

                    <div class="col-sm-offset-8 col-sm-2">
                        <div>
                            <button class="btn btn-primary" id="logout">Logout</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="message">Message Text</label>
                        <textarea id="message" cols="30" rows="10" placeholder="message"
                                  class="form-control"></textarea>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary" id="sendMessage">Send</button>
                    </div>
                    <div class="col-sm-2 col-sm-offset-8">
                        <button type="button" class="btn btn-primary" id="addPict" data-toggle='modal' data-target="#modalPICT">Add Picture</button>
                    </div>
                </div>
        </form>


        <!--Modal Adding Picture-->

        <div class="modal" id="modalPICT">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Adding the picture</h4>
                        <button class="close" type="button" data-dismiss="modal">x</button>
                    </div>
                    <div class="modal-body">
                        <p>Select picture to add:</p>
                        <form name="form" id="fileForm">
                            <input type="file" name="filename" id="filename">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success confirmFile" type="button" data-dismiss="modal">Load Picture</button>
                        <button class="btn btn-default cancelFile" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <!--Modal Adding Picture-->

        <!--Form-->
    </div>

    <div class="row">
        <hr/>
    </div>

    <!--Comments-->
    <div id="comments">
    <div class="panel-group">
    <div class="row">
    <?php

    ini_set('display_errors', E_ALL);

    require "app/Autoloader/Autoloader.php";

    $currentUser = $_SESSION['name'];

    $message = new App\Models\Message();
    $data = $message->allMessages();

    $maxVisibleLetters = 100;

    foreach ($data as $key=>$value) {

        $id      = $value['id'];
        $name    = $value['name'];
        $comment = $value['comment'];
        $date    = $value['date'];

        $toShow = substr($comment, 0, $maxVisibleLetters);
        $toHide = substr($comment, $maxVisibleLetters);

        echo "<div class='panel panel-primary comment' id='" . $id . "'>
            <div class='panel-heading'><div class='col-sm-2 messageOwner'>" . $name . "</div>
            <div class='col-sm-2 col-sm-offset-8'>" . $date . "</div><div class='clearfix'></div></div>
            <div class='panel-body'>";

        if (strlen($comment) > $maxVisibleLetters) {
            echo "<div class='toShow'>" . $toShow . "</div><span class='threeDots'> ... </span>
                <div class='toHide'>" . $toHide . "</div>";
        } else {
            echo $comment;
        }
        echo "</div><div class='panel-footer'><div class='col-sm-2'>";

        if (strlen($comment) > $maxVisibleLetters) {
            echo  "<button type='submit' class='btn btn-primary showMore'>Show More...</button>";
        }

        echo "</div><div class='col-sm-2 col-sm-offset-8'>";
            if($name == $currentUser)
            {
                echo  "<button type='button' class='btn btn-primary deleteComment'
                data-toggle='modal' data-target='#modal'>Delete</button>";
            }

          echo "</div><div class='clearfix'></div></div></div>";
    }

    ?>
        </div>
        <!--Comment-->

        </div>
        </div>

        <!--Pagination-->
        <div class="row text-center">
            <ul class="pagination">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
        </div>
        <!--Pagination-->

        </div>

        <!--Footer-->
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">Keith Chasen</div>
                <div class="panel-body">
                    <div class="col-sm-11">&copy;</div>
                    <div class="col-sm-1">2016</div>
                </div>
            </div>
        </div>


        <!--modal window-->
        <div class="modal" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Deleting Comment</h4>
                        <button class="close" type="button" data-dismiss="modal">x</button>
                    </div>
                    <div class="modal-body">
                        <p>Do you really wanna delete this comment?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger confirmDelete" type="button" data-dismiss="modal">Delete</button>
                        <button class="btn btn-default cancelDelete" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>


        <!--modal window-->
        <!---->


    <?php else: ?>

    <div class="row"><h1>Login to proceed:</h1></div>
    <div id="info">
        <div id="can_log_in" style="display: none"><p>Now you can log in:</p></div>
        <div id="sign_in_error"></div>
    </div>

    <p>Have account?:</p>
    <button class="btn btn-success" type="button" data-dismiss="modal" id="login" data-toggle='modal'
            data-target="#login-in">Login
    </button>
    <br/>
    <hr/>
    <p>New to my super guest book? Sign in:</p>
    <button class="btn btn-success" type="button" data-dismiss="modal" id="signin" data-toggle='modal'
            data-target="#sign-in">Sign In
    </button>

    <!--LOGIN Modal-->

    <div class="modal" id="login-in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">LOGIN</h4>
                    <button class="close" type="button" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body">

                    <form role="form">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" id="login_username" placeholder="Enter your username"
                                   class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="login_password" placeholder="Enter your password"
                                   class="form-control">
                        </div>

                        <div class="checkbox">
                            <label><input type="checkbox">Remember me</label>
                        </div>

                        <button type="button" class="btn btn-success" data-dismiss="modal" id="login_btn">Login</button>
                    </form>


                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <!--LOGIN Modal-->

    <!--SIGN IN Modal-->

    <div class="modal" id="sign-in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">SIGN-IN</h4>
                    <button class="close" type="button" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body">

                    <form role="form">
                        <div class="form-group">
                            <label for="sign_in_username">Username</label>
                            <input type="text" id="sign_in_username" placeholder="Enter your username"
                                   class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="sign_in_email">Email</label>
                            <input type="email" id="sign_in_email" placeholder="Enter your email" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="sign_in_password">Password</label>
                            <input type="password" id="sign_in_password" placeholder="Enter your password"
                                   class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="sign_in_repeat-password">Repeat Password</label>
                            <input type="password" id="sign_in_repeat-password" placeholder="Repeat your password"
                                   class="form-control">
                        </div>

                        <button type="button" class="btn btn-success" data-dismiss="modal" id="sign_in_btn">Sign In
                        </button>
                    </form>

                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <!--SIGN IN Modal-->


    <?php
endif;
?>

</body>
<script src="js/jquery.js"></script>
<script src="js/script.js"></script>
<script src="js/showHideDelete.js"></script>
<script src="bootstrap-3.3.6-dist/js/bootstrap.js"></script>
<script src="js/sign_in.js"></script>
<script src="js/login.js"></script>
<script src="js/log_out.js"></script>
<script src="js/files.js"></script>


</html>



$(document).ready(function () {

    $("#login_btn").click(function () {

        var login = $("#login_username").val();
        var pass  = $("#login_password").val();

        var data = {login: login, pass:  pass};

        $.ajax({
            url: "login_sign_in_scripts/login.php",
            type: "POST",
            data: data,
            dataType: "json",
            success: function (data) {
                if(data.error == null){
                    $("#sign_in_error").hide();
                    window.location.assign("index.php");
                } else {
                    $("#sign_in_error").show().html("<p style='color: red'>" + data.error + "</p>");
                }
            },
            error: function (data) {
                alert("Login Error: " + data.error);
            }
        });
    });
});

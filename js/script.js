var maxVisibleLetters = 100;

$(document).ready(function () {
    //Adding Article To DB
    $("body").on("click", "#sendMessage", function () {
        var name = $.trim($("#userName").text());
        var email = $.trim($("#userEmail").text());
        var message = $("#message").val();

        var data = {name: name, message: message, email: email};

        $.ajax({
            url: "addMessage.php",
            type: "POST",
            data: data,
            dataType: "json",
            success: function (response) {

                var message = response.message;
                var id      = response.id;
                var name    = response.name;
                var date    = response.date;

                var commentHTML;

                if (response.errorMessage == null) {

                    var toShow = message.substr(0, maxVisibleLetters);
                    var toHide = message.substr(maxVisibleLetters);

                        commentHTML = "<div class='panel panel-primary comment' id=" + id + ">"
                                    + "<div class='panel-heading'>"
                                    + "<div class='col-sm-2 messageOwner'>" + name + "</div>"
                                    + "<div class='col-sm-2 col-sm-offset-8'>" + date + "</div>"
                                    + "<div class='clearfix'></div></div>"
                                    + "<div class='panel-body'>";

                        if (message.length > maxVisibleLetters) {

                            commentHTML += "<div class='toShow'>"
                                        + toShow
                                        + "</div>"
                                        + "<span class='threeDots'> ... </span>"
                                        + "<div class='toHide'>"
                                        + toHide
                                        + "</div>";

                        } else {
                                    commentHTML += message;
                            }

                            commentHTML +=  "</div>"
                                        + "<div class='panel-footer'>"
                                        + "<div class='col-sm-2'>";

                    if (message.length > maxVisibleLetters) {

                        commentHTML += "<button type='submit' class='btn btn-primary showMore'>"
                                    + "Show More...</button>";

                    }

                        commentHTML += "</div><div class='col-sm-2 col-sm-offset-8'>"
                                    + "<button type='button' class='btn btn-primary "
                                    + "deleteComment' data-toggle='modal' data-target='#modal'>Delete</button></div>"
                                    + "<div class='clearfix'></div>"
                                    + "</div></div>";

                    $("#comments .panel-group .row").append(commentHTML);

                }

                else {
                    alert(response.errorMessage);
                }

                $("textarea").val("");
                return false;
            }
        });
        return false;
    })

    //Deleting message:

    .on("click", ".deleteComment", function () {
        var path = $(this).parents(".comment");
        var id = path.attr("id");

        path.toggleClass("toDel");

        $(".confirmDelete").click(function () {
            $.ajax({
                url: "delMessage.php",
                type: "POST",
                data: {id: id},
                success: function () {
                        $(".toDel").remove();
                },
                error: function () {
                    alert("Deleting Error!");
                }
            });


        });

        $("body").on("click", ".cancelDelete", function () {
            //alert("Cancel Del");
            path.removeClass("toDel");
        });
    });
});


$(document).ready(function () {

    ////Function Show More

    $('body').on("click", ".showMore", function () {
        $(this).parents(".comment").find(".toHide").slideToggle("slow");
        $(this).parents(".comment").find(".threeDots").slideToggle("slow");

        if($(this).html() == "Show More..."){
            $(this).animate({opacity: 0.5}, function () {
                $(this).html("Less").animate({opacity: 1});
            });
        } else {
            $(this).animate({opacity: 0.5}, function () {
                $(this).html("Show More...").animate({opacity: 1});
            });
        }
    });
});
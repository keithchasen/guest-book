$(document).ready(function () {

    //Sign-in:

    $("body").on("click", "#sign_in_btn", function () {

        var name        = $("#sign_in_username").val();
        var email       = $("#sign_in_email").val();
        var pass        = $("#sign_in_password").val();
        var passConfirm = $("#sign_in_repeat-password").val();

        var data = {
            name: name,
            email: email,
            pass: pass,
            passConfirm: passConfirm
        };

        $.ajax({
            url: "login_sign_in_scripts/sign_in.php",
            type: "POST",
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.error == null) {
                    $("#can_log_in").show();
                    $("#sign_in_error").hide();
                } else {
                    $("#can_log_in").hide();
                    $("#sign_in_error").show();
                    $("#sign_in_error").html("<p style='color: red'>" + response.error + "</p>");
                }
            },
            error: function (response) {
                alert("Error: " + response.error);
            }
        });
    });

});
<?php

//use Models\UserModel;
ini_set('display_errors', E_ALL);

require "../app/Autoloader/Autoloader.php";

session_start();

$login = htmlspecialchars($_POST['login'], ENT_QUOTES);
$pass  = htmlspecialchars($_POST['pass'], ENT_QUOTES);
$error = null;

if (strlen($login) < 1) {
    $error = "Enter Login. It should contain at least 1 character";
} elseif (strlen($pass) < 6) {
    $error = "Password should contain at least 6 characters";
}

if ($error == null) {
    $user = new App\Models\User();
    $error = $user->logIn($login, $pass);
}

echo json_encode(["error" => $error]);

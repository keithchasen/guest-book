<?php

require "../app/Autoloader/Autoloader.php";

//data from form:

$name        = htmlspecialchars($_POST['name'], ENT_QUOTES);
$email       = htmlspecialchars($_POST['email'], ENT_QUOTES);
$pass        = htmlspecialchars($_POST['pass'], ENT_QUOTES);
$passConfirm = htmlspecialchars($_POST['passConfirm'], ENT_QUOTES);

$errorMessage = null;

if (strlen($name) < 1) {
    $errorMessage = "Enter Login. It should contain at least 1 character";

} elseif (strlen($pass) < 6) {
    $errorMessage = "Password should contain at least 6 characters";
} elseif ($pass != $passConfirm) {
    $errorMessage = "Entered passwords don't match!";
} elseif (empty($email || !filter_var($email, FILTER_VALIDATE_EMAIL))) {
    $errorMessage = "Enter valid email";
} else {

    $user = new App\Models\User();
    $errorMessage = $user->signIn($name, $email, $pass);
}

echo json_encode(["error" => $errorMessage]);





